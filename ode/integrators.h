//
// Created by msmetko on 09. 01. 2020..
//

#ifndef APR_INTEGRATORS_H
#define APR_INTEGRATORS_H

#include <functional>
#include <memory>
#include "../linalg/linalg.h"

typedef std::vector<Matrix> matrix_coef_list;
typedef std::function<Matrix(const double)> input_function;
typedef std::function<Matrix(const matrix_coef_list&, const Matrix&, double, double,
                             const input_function&)> integration_step;

class Integrator {
public:
    virtual Matrix step() = 0;
};

class LinearIntegrator : Integrator {
protected:
    matrix_coef_list coefs_;
    Matrix current;
    input_function f_;
    const double start_;
    const double step_;
    int call_count;
    integration_step step_f;

    LinearIntegrator(matrix_coef_list, const Matrix&, const input_function&, double start, double);

    virtual Matrix perform(const matrix_coef_list&, const Matrix&, double, double, const input_function&);

public:
    Matrix step() final override {
        current = perform(coefs_, current, start_ + call_count * step_, step_, f_);
        call_count += 1;
        return current;
    }
};

class Euler : public LinearIntegrator {
public:
    Euler(const Matrix&, const Matrix&, const Matrix&, const input_function&, double, double);
};

class BackwardEuler : public LinearIntegrator {
public:
    BackwardEuler(const Matrix&, const Matrix&, const Matrix&, const input_function&, double, double);
};

class Trapezoidal : public LinearIntegrator {
public:
    Trapezoidal(const Matrix&, const Matrix&, const Matrix&, const input_function&, double, double);
};

class RK4 : public LinearIntegrator {
public:
    RK4(const Matrix&, const Matrix&, const Matrix&, const input_function&, double, double);
};

class PECE : public LinearIntegrator {
private:
    Matrix A;
    Matrix B;
public:
    PECE(const Matrix&, const Matrix&, const Matrix&, const input_function&, double, double);

    Matrix perform(const matrix_coef_list&, const Matrix&, double, double, const input_function&) override;
};

class PECECE : public LinearIntegrator {
private:
    Matrix A;
    Matrix B;
public:
    PECECE(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f, double start, double step);

    Matrix perform(const matrix_coef_list&, const Matrix&, double, double, const input_function&) override;
};

#endif //APR_INTEGRATORS_H
