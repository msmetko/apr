#include "integrators.h"

matrix_coef_list euler_matrices(const Matrix& A, const Matrix& B, const double step) {
    // row count == column count
    return {Matrix::eye(A.getRowCount()) + A * step, B * step};
}

integration_step euler_step = [](const matrix_coef_list& coefList, const Matrix& x_k, const double t, const double step,
                                 const input_function& f) {
    return coefList[0] * x_k + coefList[1] * f(t);
};

matrix_coef_list backward_euler_matrices(const Matrix& A, const Matrix& B, const double step) {
    Matrix T = ~(Matrix::eye(A.getRowCount()) - step * A);
    return {T, T * step * B};
}

integration_step backward_euler_step = euler_step;

matrix_coef_list trapezoidal_matrices(const Matrix& A, const Matrix& B, const double step) {
    Matrix I = Matrix::eye(A.getRowCount());
    Matrix T2A = A * (step / 2.0);
    Matrix inv = ~(I - T2A);
    return {inv * (I + T2A), inv * B * (step / 2.0)};
}

integration_step trapezoidal_step = [](const matrix_coef_list& coefList, const Matrix& x_k, const double t,
                                       const double step, const input_function& f) {
    Matrix r = f(t);
    r += f(t + step);
    return coefList[0] * x_k + coefList[1] * r;
};

matrix_coef_list rk4_matrices(const Matrix& A, const Matrix& B, const double step) {
    Matrix I = Matrix::eye(A.getRowCount());
    Matrix M1 = I;
    Matrix M2 = I;
    Matrix M3 = I * 4;
    Matrix mul = A * step;
    Matrix T = mul;

    M1 += T;
    M2 += T;
    M3 += T;
    M3 += T;
    T = T * mul / 2.0;

    M1 += T;
    M2 += T;
    M3 += T;
    T = T * mul;

    M1 += T / 3.0;
    M2 += T / 2.0;

    T = T * mul / 12.0;
    M1 += T;

    return {M1, M2 * B, M3 * B, B};
}

integration_step rk4_step = [](const matrix_coef_list& coefList, const Matrix& x_k, const double t, const double step,
                               const input_function& f) {
    Matrix r = coefList[1] * f(t);
    r += coefList[2] * f(t + step / 2.0);
    r += coefList[3] * f(t + step);
    r *= step / 6.0;
    r += coefList[0] * x_k;
    return r;
};

LinearIntegrator::LinearIntegrator(matrix_coef_list list, const Matrix& x_0, const input_function& f,
                                   const double start, const double step) : current(x_0), f_(f), start_(start),
                                                                            step_(step),
                                                                            call_count(0), coefs_(list) {}

Matrix LinearIntegrator::perform(const matrix_coef_list& coefs, const Matrix& x_k, const double t, const double step,
                                 const input_function& f) {
    return step_f(coefs, x_k, t, step, f);
}

Euler::Euler(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f, const double start,
             const double step) : LinearIntegrator(euler_matrices(A, B, step), x_0, f, start, step) {
    step_f = euler_step;
}

BackwardEuler::BackwardEuler(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f,
                             const double start,
                             const double step) :
        LinearIntegrator(backward_euler_matrices(A, B, step), x_0, f, start, step) {
    step_f = backward_euler_step;
}

Trapezoidal::Trapezoidal(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f,
                         const double start,
                         const double step) :
        LinearIntegrator(trapezoidal_matrices(A, B, step), x_0, f, start, step) {
    step_f = trapezoidal_step;
}

RK4::RK4(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f, const double start,
         const double step) :
        LinearIntegrator(rk4_matrices(A, B, step), x_0, f, start, step) {
    step_f = rk4_step;
}

PECE::PECE(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f, const double start,
           const double step) :
        LinearIntegrator(euler_matrices(A, B, step), x_0, f, start, step), A(A), B(B) {
    step_f = euler_step;
}

Matrix PECE::perform(const matrix_coef_list& coefs, const Matrix& x_k, const double t, const double step,
                     const input_function& f) {
    Matrix prediction = step_f(coefs, x_k, t, step, f);
    return x_k + (A * (x_k + prediction) + B * (f(t) + f(t + step))) * step / 2.0;
}

PECECE::PECECE(const Matrix& A, const Matrix& B, const Matrix& x_0, const input_function& f, const double start,
               const double step) : LinearIntegrator(euler_matrices(A, B, step), x_0, f, start, step), A(A), B(B) {
    step_f = euler_step;
}

Matrix PECECE::perform(const matrix_coef_list& coefs, const Matrix& x_k, const double t, const double step,
                       const input_function& f) {
    Matrix prediction = step_f(coefs, x_k, t, step, f);
    Matrix correction = x_k + step * (A * prediction + B * f(t + step));
    return x_k + step * (A * correction + B * f(t + step));
}
