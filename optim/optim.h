//
// Created by msmetko on 22. 11. 2019..
//

#ifndef APR_OPTIM_H
#define APR_OPTIM_H

#include "../function/Function.h"

double golden_section_search(Function<double, double>&, double, double, double);

double golden_section_search(Function<double, double>&, double, double= 1e-6);

Matrix coordinate_descent(Function<Matrix, double>&, Matrix&, double= 1e-6);

Matrix
amoeba_simplex(Function<Matrix, double>&, Matrix&, double= 1.0, double= 1.0, double= 0.5, double= 2.0, double= 0.5,
               double= 1e-6);

Matrix hooke_jeeves_pattern_search(Function<Matrix, double>&, const Matrix&, Matrix, double= 1e-6);

#endif //APR_OPTIM_H
