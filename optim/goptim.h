//
// Created by msmetko on 22. 11. 2019..
//

#ifndef APR_GOPTIM_H
#define APR_GOPTIM_H

#include "../function/Function.h"

Matrix gradient_descent(Function<Matrix, double>&, Function<Matrix, Matrix>&, Matrix&, bool= false, double= 1e-6);

Matrix
newton_rhapson(Function<Matrix, double>&, Function<Matrix, Matrix>&, Function<Matrix, Matrix>&, Matrix&, bool= false,
               double= 1e-6);

Matrix box_constraint(Function<Matrix, double>&, Matrix&, Matrix&, Matrix&, std::vector<std::function<double(Matrix)>>,
                      double= 1.3, double= 1e-6);

Matrix mix_transform(Function<Matrix, double>&, Matrix&, std::vector<std::function<double(Matrix)>>&,
                     std::vector<std::function<double(Matrix)>>&, double= 1e-6);

#endif //APR_GOPTIM_H
