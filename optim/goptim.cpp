//
// Created by msmetko on 22. 11. 2019..
//

#include <random>
#include "goptim.h"
#include "optim.h"

double norm(Matrix x) {
    return std::sqrt((x.T() * x)(0, 0));
}

Matrix gradient_descent(Function<Matrix, double>& function, Function<Matrix, Matrix>& grad, Matrix& x0, bool search_min,
                        double tol) {
    Matrix x{x0};
    Matrix gx = grad(x);
    while (norm(gx) > tol) {
        if (search_min) {
            Function<double, double> lambda_f([&](double lambda) {
                return function(x + lambda * gx);
            });
            double min_lambda = golden_section_search(lambda_f, 0);
            x += min_lambda * gx;
        } else {
            x -= 0.1 * gx;
        }
        gx = grad(x);
    }
    return x;
}

Matrix
newton_rhapson(Function<Matrix, double>& function, Function<Matrix, Matrix>& grad, Function<Matrix, Matrix>& hess,
               Matrix& x0, bool search_min, double tol) {
    Matrix x{x0};
    Matrix gx = grad(x);
    while ((norm(gx)) > std::sqrt(tol)) {
        Matrix H = hess(x);
        Matrix dx = Matrix::solve(H, gx);
        if (search_min) {
            dx /= norm(dx);
            Function<double, double> lambda_f([&](double lambda) {
                return function(x + lambda * dx);
            });
            double min_lambda = golden_section_search(lambda_f, 0, tol = 1e-6);
            x += min_lambda * dx;
        } else {
            x -= dx;
        }
//        std::cout << x << std::endl;
        gx = grad(x);
    }
    return x;
}

Matrix box_constraint(Function<Matrix, double>& function, Matrix& x0, Matrix& x_l, Matrix& x_u,
                      std::vector<std::function<double(Matrix)>> constraints, double a, double tol) {
    int n = x0.getRowCount(); // dimension
    for (int i = 0; i < n; ++i) {
        if (x0(i, 0) < x_l(i, 0) or x0(i, 0) > x_u(i, 0)) {
            return Matrix{{{NAN}, {NAN}}};
        }
    }
    for (auto c: constraints) {
        if (c(x0) < 0) return Matrix{{{NAN}, {NAN}}};
    }
    Matrix xc{x0};
    std::default_random_engine generator;
    std::uniform_real_distribution distribution;
    std::vector<Matrix> points;
    points.reserve(2 * n);
    for (int i = 0; i < 2 * n; ++i) {
        // R vector
        points.emplace_back(n, 1);
        for (int j = 0; j < n; ++j) {
            points[i](j, 0) = x_l(j, 0) + distribution(generator) * (x_u(j, 0) - x_l(j, 0));
        }
        // r initialized
        while (!std::all_of(constraints.begin(), constraints.end(),
                            [&](std::function<double(Matrix)> f) { return f(points[i]) >= 0; })) {
            points[i] = 0.5 * (points[i] + xc);
        }
    }
//    for (auto x: points) {
//        std::cout << x << std::endl;
//    }
    for (int i = 0; i < 10000; ++i) {
        std::sort(points.begin(),
                  points.end(),
                  [&](const Matrix& a, const Matrix& b) { return function(a) < function(b); });
        int h2 = points.size() - 1;
        int h = h2--;
        if (function(points[h]) - function(points[0]) < tol) break;
        xc = std::accumulate(points.begin(), points.end() - 1, Matrix{{{0.0}, {0.0}}},
                             [](Matrix& a, const Matrix& b) { return a += b; });
        xc /= (points.size() - 1);
        Matrix xr = (1 + a) * xc - a * points[h];
        for (int j = 0; j < n; ++j) {
            if (xr(j, 0) < x_l(j, 0)) xr(j, 0) = x_l(j, 0);
            if (xr(j, 0) > x_u(j, 0)) xr(j, 0) = x_u(j, 0);
        }
        while (!std::all_of(constraints.begin(), constraints.end(),
                            [&](std::function<double(Matrix)> f) { return f(xr) >= 0; })) {
            xr = 0.5 * (xr + xc);
        }
        if (function(xr) > function(points[h2])) {
            xr = 0.5 * (xr + xc);
        }
        points[h] = xr;
    }
    return points[0];
}

Matrix mix_transform(Function<Matrix, double>& function, Matrix& x0, std::vector<std::function<double(Matrix)>>& g,
                     std::vector<std::function<double(Matrix)>>& h, double tol) {
    Matrix previous_best{x0};
    Matrix x{x0};
    double t = 1.0;
    do {
//        std::cout << t << std::endl;
        previous_best = x;
        Function<Matrix, double> fun{[&](const Matrix& x) {
            double ineq = std::accumulate(g.begin(), g.end(), 0.0, [&](double sum, auto f) {
                double temp = f(x);
                return sum - temp > (tol * tol) ? std::log(temp) : HUGE_VAL;
            });
            double eq = std::accumulate(h.begin(), h.end(), 0.0, [&](double sum, auto f) {
                double temp = f(x);
                return sum + temp * temp;
            });
            return function(x) + ineq / t + eq * t;
        }};
//        Matrix dx{{{1.0}, {1.0}}};
//        x = hooke_jeeves_pattern_search(fun, x, dx);
        x = amoeba_simplex(fun, x);
        t *= 10.0;
//std::cout << x << std::endl;
//        std::cout << "TEST\n" << x << std::endl << previous_best << "not test\n"<<std::endl;
    } while (norm(x - previous_best) > tol);
    return x;
}