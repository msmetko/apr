//
// Created by msmetko on 22. 11. 2019..
//

#include <random>
#include "optim.h"

/**  The smaller of two golden numbers, used for for multiplication as opposed to the division */
const double PHI = 0.6180339887498948482; // This is maximum number of decimal places we can use due to imprecision

std::tuple<double, double> find_unimodal(Function<double, double>& f, double x0, double h) {
    double m = x0;
    double l = m - h;
    double r = m + h;
    int step = 1;
    double fm = f(m);
    double fl = f(l);
    double fr = f(r);
    if (fm > fr) {
        do {
            l = m;
            m = r;
            fm = fr;
            r = x0 + h * (step *= 2);
            fr = f(r);
        } while (fm > fr);
    } else if (fm > fl) {
        do {
            r = m;
            m = l;
            fm = fl;
            l = x0 - h * (step *= 2);
            fl = f(l);
        } while (fm > fl);
    }
    return std::tuple(l, r);
}

double golden_section_search(Function<double, double>& f, double a, double b, double tol) {
    double c = b - PHI * (b - a);
    double d = a + PHI * (b - a);
    double fc = f(c);
    double fd = f(d);
    while ((b - a) > tol) {
//        std::cout << "a\tc\td\tb\n" << a << "\t" << c << "\t" << d << "\t" << b;
//        std::cout << "\n\t" << fc << "\t" << fd << std::endl;
        if (fc < fd) {
            b = d;
            d = c;
            c = b - PHI * (b - a);
            fd = fc;
            fc = f(c);
        } else {
            a = c;
            c = d;
            d = a + PHI * (b - a);
            fc = fd;
            fd = f(d);
        }
    }
    return (a + b) / 2.0;
}

double golden_section_search(Function<double, double>& f, double x0, double tol) {
    auto bounds = find_unimodal(f, x0, 1.0);
//    std::cout << "Bounds: " << std::get<0>(bounds) << ", " << std::get<1>(bounds) << std::endl;
    return golden_section_search(f, std::get<0>(bounds), std::get<1>(bounds), tol);
}

Matrix coordinate_descent(Function<Matrix, double>& f, Matrix& x0, double tol) {
    Matrix x(x0);
    Matrix xs = x;
    do {
        xs = x;
        for (int i = 0; i < x0.getRowCount(); ++i) {
            std::vector<std::vector<double>> m(x0.getRowCount(), std::vector<double>(1, 0.0));
            Matrix e_i(m);
            e_i(i, 0) = 1.0;
            Function<double, double> function([&](double lambda) { return f(x + lambda * e_i); });
            double min_lambda = golden_section_search(function, 0.01);
            x = x + min_lambda * e_i;
//            std::cout << x << std::endl;
        }
    } while (sqrt(((xs - x).T() * (xs - x))(0, 0)) > tol);
    return x;
}

Matrix amoeba_simplex(Function<Matrix, double>& f, Matrix& x0, double step, double alpha, double beta, double gamma,
                      double sigma, double tol) {
    int n = x0.getRowCount();
    std::vector<Matrix> simplex;
    simplex.reserve(n);
    simplex.push_back(x0);
    for (int i = 0; i < n; ++i) {
        Matrix x_n(x0);
        x_n(i, 0) += step;
        simplex.push_back(x_n);
    }
//    int iter = 0;
    // SIMPLEX INITIALIZED (simplex.size() == n+1)
    for (int epochs = 0; epochs < 10000; epochs++) {
        if (sqrt(((simplex[0] - simplex[n]).T() * (simplex[0] - simplex[n]))(0, 0)) < tol) break;

        // SORTING SIMPLEX
        std::sort(simplex.begin(),
                  simplex.end(),
                  [&](Matrix& a, Matrix& b) { return f(a) < f(b); });
        Matrix& l = simplex[0];
        double f_l = f(l);
        Matrix& s = simplex[n - 1];
        double f_s = f(s);
        Matrix& h = simplex[n];
        double f_h = f(h);
        // CALCULATING THE CENTROID USING KNUTH's ONLINE ALGORITHM
        Matrix centroid(simplex[0]);
        for (int i = 1; i < n; ++i) {
            double denom = i + 1;
            centroid *= i / denom;
            centroid += (simplex[i] / denom);
        }
        // REFLECTION
        Matrix r = centroid + alpha * (centroid - h);
        double f_r = f(r);
        if (f_l <= f_r && f_r <= f_s) {
            simplex[n] = r;
//        continue;
        } else if (f_r < f_l) {
            // EXPANSION
            Matrix e = centroid + gamma * (r - centroid);
            double f_e = f(e);
            if (f_e < f_r) {
                simplex[n] = e;
            } else {
                simplex[n] = r;
            }
        } else {
            // CONTRACT
            // let's find which of the point r and point l is better for contraction
            Matrix& around = f_r < f_h ? r : h;
            double f_around = f_r < f_h ? f_r : f_h;
            Matrix c = centroid + beta * (around - centroid);
            double f_c = f(c);
            if (f_c < f_around) {
                simplex[n] = c;
            } else {
                // SHRINKING
                for (auto& item : simplex) {
                    item += sigma * (simplex[0] - item);
                }
            }
        }
    }
    return simplex[0];
}

Matrix hooke_jeves_explore(Function<Matrix, double>& f, const Matrix& x_p, const Matrix& dx) {
    Matrix x(x_p);
    for (int i = 0; i < x_p.getRowCount(); ++i) {
        double P = f(x);
        x(i, 0) += dx(i, 0);
        double N = f(x);
        if (N > P) {
            x(i, 0) -= 2 * dx(i, 0);
            N = f(x);
            if (N > P) {
                x(i, 0) += dx(i, 0);
            }
        }
    }
    return x;
}

Matrix hooke_jeeves_pattern_search(Function<Matrix, double>& F, const Matrix& x0, Matrix dx, double tol) {
    Matrix x_p(x0);
    Matrix x_b(x_p);
    do {
        Matrix x_n = hooke_jeves_explore(F, x_p, dx);
        if (F(x_n) < F(x_b)) {
            x_p = 2 * x_n - x_b;
            x_b = x_n;
        } else {
            x_p = x_b;
            dx /= 2.0;
        }
    } while (sqrt((dx.T() * dx)(0, 0)) > tol);
    return x_b;
}