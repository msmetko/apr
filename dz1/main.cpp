#include <iostream>
#include "../linalg/Matrix.h"

void zadatak1() {
    std::cout << "1. (LAB1)\n";
    Matrix M({{0.00000001,         200000000.045},
              {3.1415926535897932, 1.414294738}});
    Matrix A(M);
    A *= 3.141592653589793;
    A /= 10.0;
    A *= 0.01 / 0.0031415926535897932;
    A -= M;
    std::cout << "Difference:\n" << A << std::endl;
}

void zadatak2() {
    std::cout << "2. (LAB1)\n";
    Matrix A = Matrix::fromFile("2_A.txt");
    Matrix b = Matrix::fromFile("2_b.txt");
    std::cout << "Matrica:\n" << A;
    std::cout << "Slobodni vektor" << b;
    std::cout << "LU\n";
    try {
        std::cout << Matrix::solve(A, b);
    } catch (std::runtime_error&) {
        std::cout << "Runtime error - division by zero!\n";
    }
    std::cout << "LUP\n" << Matrix::solve(A, b, true) << std::endl;
}

void zadatak3() {
    std::cout << "3. (LAB1)\n";
    Matrix A = Matrix::fromFile("3_A.txt");
    std::cout << "Matrica:\n" << A;
    std::cout << "Determinanta je " << A.det();
}

void zadatak4() {
    std::cout << "4. (LAB1)\n";
    Matrix A = Matrix::fromFile("4_A.txt");
    Matrix b = Matrix::fromFile("4_b.txt");
    std::cout << "Matrica\n" << A;
    std::cout << "Vektor\n" << b;
    std::cout << "LU\n" << Matrix::solve(A, b);
    std::cout << "LUP\n" << Matrix::solve(A, b, true);
}

void zadatak5() {
    std::cout << "5. (LAB1)\n";
    Matrix A = Matrix::fromFile("5_A.txt");
    Matrix b = Matrix::fromFile("5_b.txt");
    std::cout << "Matrica\n" << A;
    std::cout << "Vektor\n" << b;
    std::cout << "LU\n";
    try {
        std::cout << Matrix::solve(A, b);
    } catch (...) {
        std::cout << "Error!";
    }
    std::cout << "LUP\n" << Matrix::solve(A, b, true);
}

void zadatak6() {
    std::cout << "6. (LAB1)\n";
    Matrix A = Matrix::fromFile("6_A.txt");
    Matrix b = Matrix::fromFile("6_b.txt");
    std::cout << "Matrica\n" << A;
    std::cout << "Vektor\n" << b;
    std::cout << "LU\n" << Matrix::solve(A, b);
    std::cout << "LUP\n" << Matrix::solve(A, b, true);
}

void zadatak7() {
    std::cout << "7. (LAB1)\n";
    Matrix A = Matrix::fromFile("3_A.txt");
    std::cout << "A:\n" << A;
    std::cout << "~A:\n" << ~A;
}

void zadatak8() {
    std::cout << "8. (LAB1)\n";
    Matrix A = Matrix::fromFile("8_A.txt");
    std::cout << "A:\n" << A;
    std::cout << "~A:\n" << ~A;
}

void zadatak9() {
    std::cout << "9. (LAB1)\n";
    Matrix A = Matrix::fromFile("8_A.txt");
    std::cout << "A:\n" << A;
    std::cout << "|A|: " << A.det();
}

void zadatak10() {
    std::cout << "10. (LAB1)\n";
    Matrix A = Matrix::fromFile("2_A.txt");
    std::cout << "A:\n" << A;
    std::cout << "|A|: " << A.det();
}

int main() {
    zadatak1();
    zadatak2();
    zadatak3();
    zadatak4();
    zadatak5();
    zadatak6();
    zadatak7();
    zadatak8();
    zadatak9();
    zadatak10();
}