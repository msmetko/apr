#include <iostream>
#include "../optim/goptim.h"

/* FUNCTION 1 */
Function<Matrix, double> f_1([](const Matrix& x) {
    return 100 * (x(1, 0) - x(0, 0) * x(0, 0)) * (x(1, 0) - x(0, 0) * x(0, 0)) + (1 - x(0, 0)) * (1 - x(0, 0));
});

Function<Matrix, Matrix> gf_1([](const Matrix& x) {
    return Matrix{{
                          {2 * (200 * x(0, 0) * x(0, 0) * x(0, 0) - 200 * x(0, 0) * x(1, 0) + x(0, 0) - 1)},
                          {200 * (x(1, 0) - x(0, 0) * x(0, 0))}
                  }};
});

Function<Matrix, Matrix> hf_1([](const Matrix& x) {
    return Matrix{{
                          {1200 * x(0, 0) * x(0, 0) - 400 * x(1, 0) + 2, -400 * x(0, 0)},
                          {-400 * x(0, 0), 200}
                  }};
});

/* FUNCTION 2 */
Function<Matrix, double> f_2([](const Matrix& x) {
    return (x(0, 0) - 4) * (x(0, 0) - 4) + 4 * (x(1, 0) - 2) * (x(1, 0) - 2);
});

Function<Matrix, Matrix> gf_2([](const Matrix& x) {
    return Matrix{{
                          {2 * (x(0, 0) - 4)},
                          {8 * (x(1, 0) - 2)}
                  }};
});

Function<Matrix, Matrix> hf_2([](const Matrix& x) {
    return Matrix{{
                          {2.0, 0.0},
                          {0.0, 8.0}
                  }};
});


/* FUNCTION 3 */
Function<Matrix, double> f_3([](const Matrix& x) {
    return (x(0, 0) - 2) * (x(0, 0) - 2) + (x(1, 0) + 3) * (x(1, 0) + 3);
});

Function<Matrix, Matrix> gf_3([](const Matrix& x) {
    return Matrix{{
                          {2 * (x(0, 0) - 2)},
                          {2 * (x(1, 0) + 3)}
                  }};
});

Function<Matrix, Matrix> hf_3([](const Matrix&) {
    return Matrix{{
                          {2.0, 0.0},
                          {0.0, 2.0}
                  }};
});

/* FUNCTION 4 */
Function<Matrix, double> f_4([](const Matrix& x) {
    return (x(0, 0) - 3) * (x(0, 0) - 3) + x(1, 0) * x(1, 0);
});

Function<Matrix, Matrix> gf_4([](const Matrix& x) {
    return Matrix{{
                          {2 * (x(0, 0) - 3)},
                          {2 * x(1, 0)}
                  }};
});


bool zadatak1() {
    Matrix x0{{{0.0}, {0.0}}};
    Matrix sol = gradient_descent(f_3, gf_3, x0);
    Matrix sol_opt = gradient_descent(f_3, gf_3, x0, true);
    std::cout << sol << std::endl << sol_opt << std::endl;
    return true;
}

bool zadatak2() {
//    Matrix x0{{{-1.9}, {2.0}}};
    Matrix x0{{{0.1}, {0.3}}};
//    Matrix sol = newton_rhapson(f_1, gf_1, hf_1, x0);
    Matrix sol = newton_rhapson(f_2, gf_2, hf_2, x0);
    std::cout << sol << std::endl;
    f_1.resetCallCount();
//    sol = newton_rhapson(f_1, gf_1, hf_1, x0, true);
    sol = newton_rhapson(f_2, gf_2, hf_2, x0, true);
    std::cout << sol << std::endl;
    return true;
}

bool zadatak3() {
    std::function<double(Matrix)> c1 = [](const Matrix& x) { return x(1, 0) - x(0, 0); };
    std::function<double(Matrix)> c2 = [](const Matrix& x) { return 2 - x(0, 0); };
    Matrix x_l{{{-100}, {-100}}};
    Matrix x_u{{{100}, {100}}};

//    Matrix x0{{{0}, {10.0}}};
//    Matrix x0{{{-1.9}, {2.0}}};
//    Matrix sol = box_constraint(f_1, x0, x_l, x_u, {c1, c2});
    Matrix x0{{{0.1}, {0.3}}};
    Matrix sol = box_constraint(f_2, x0, x_l, x_u, {c1, c2});

    std::cout << sol;
    return true;
}

bool zadatak4() {
    std::vector<std::function<double(Matrix)>> g = {
            [](const Matrix& x) { return x(1, 0) - x(0, 0); },
            [](const Matrix& x) { return 2 - x(0, 0); }
    };
    std::vector<std::function<double(Matrix)>> h = {};

//    Matrix x0{{{0.0}, {10.0}}};
//    Matrix x0{{{-1.9}, {2.0}}};
//    Matrix sol = mix_transform(f_1, x0, g, h);
    Matrix x0{{{0.1}, {0.3}}};
    Matrix sol = mix_transform(f_2, x0, g, h);

    std::cout << std::endl << sol << std::endl;
//    return true;
}

bool zadatak5() {
    std::vector<std::function<double(Matrix)>> g = {
            [](const Matrix& x) { return 3 - x(0, 0) - x(1, 0); },
            [](const Matrix& x) { return 3 + 1.5 * x(0, 0) - x(1, 0); }
    };
    std::vector<std::function<double(Matrix)>> h = {
            [](const Matrix& x) { return x(1, 0) - 1; }
    };
    Matrix x0{{{0.0}, {0.0}}};
    Matrix sol = mix_transform(f_4, x0, g, h);
    std::cout << sol << std::endl;
//    return true;
}

int main() {
//    return !zadatak1();
//    return !zadatak2();
//    return !zadatak3();
//    return !zadatak4();
    return !zadatak5();
}