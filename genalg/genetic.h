//
// Created by msmetko on 12. 12. 2019..
//

#ifndef APR_GENETIC_H
#define APR_GENETIC_H

#include <random>
#include "../linalg/Matrix.h"
#include "../function/Function.h"

class Combiner {
public:
    virtual Matrix combine(const Matrix&, const Matrix&) = 0;
};

class LinearCombiner : public Combiner {
private:
    std::uniform_real_distribution<double> distribution;
public:
    LinearCombiner(double min = 0.0, double max = 1.0) : distribution(std::uniform_real_distribution(min, max)) {}

    Matrix combine(const Matrix&, const Matrix&) override;
};

class HeuristicCombiner : public Combiner {
private:
    std::uniform_real_distribution<double> distribution;
public:
    HeuristicCombiner() : distribution(std::uniform_real_distribution()) {};

    Matrix combine(const Matrix&, const Matrix&) override;
};

class OnePointCombiner : public Combiner {
private:
    std::uniform_int_distribution<int> distribution;
public:
    OnePointCombiner(int precision) : distribution(std::uniform_int_distribution(0, precision)) {}

    Matrix combine(const Matrix&, const Matrix&) override;
};

class MultiPointCombiner : public Combiner {
private:
    std::uniform_int_distribution<int> distribution;
public:
    MultiPointCombiner(int precision) : distribution(std::uniform_int_distribution(0, 1 << precision)) {}

    Matrix combine(const Matrix&, const Matrix&) override;
};

class Genetic {
private:
    std::reference_wrapper<Combiner> combiner;
protected:
    Matrix combine(const Matrix& a, const Matrix& b) const {
        return combiner.get().combine(a, b);
    }

    std::vector<int> sample_3(std::vector<double>&) const;

    virtual std::vector<Matrix> generate(int, int, double, double) const = 0;

    virtual void mutate(Matrix&) const = 0;

    virtual Matrix evaluate(Matrix, double min, double max) const = 0;

public:
    Genetic(Combiner& c) : combiner(std::reference_wrapper(c)) {
    }

    Matrix
    optimize(const std::function<double(const Matrix&)>&, int, int, std::pair<double, double>, double, bool) const;
};

class RealGenetic : public Genetic {
protected:
    virtual std::vector<Matrix> generate(int, int, double, double) const override;

    virtual void mutate(Matrix&) const override;

    virtual Matrix evaluate(Matrix a, double, double) const override;

public:
    RealGenetic(Combiner& c) : Genetic(c) {}
};

class BinaryGenetic : public Genetic {
private:
    int precision;
protected:
    virtual std::vector<Matrix> generate(int, int, double, double) const override;

    virtual void mutate(Matrix&) const override;

    virtual Matrix evaluate(Matrix, double, double) const override;

public:
    BinaryGenetic(Combiner& c, int precision = 16) : Genetic(c), precision(precision) {}
};
#endif //APR_GENETIC_H
