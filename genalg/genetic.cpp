//
// Created by msmetko on 12. 12. 2019..
//

#include "genetic.h"
#include <random>

#define PRINT(A) std::cout << '[';for (auto x: A) {std::cout << " " << x;}std::cout << ']' << std::endl;

std::default_random_engine GENERATOR(std::random_device{}());

std::vector<double> goodness(const std::vector<double>& evaluations) {
    std::vector<double> gs(evaluations.size());
    const auto[min, max] = std::minmax_element(evaluations.begin(), evaluations.end());
    std::transform(evaluations.begin(), evaluations.end(), gs.begin(), [&, min = *min, max = *max](const double x) {
        double l = 0.70710678118654752440;
//        double l = 1;
        double u = 1.41421356237309504880;
//        double u = 2;
        double fraction = (max - x) / (max - min);
//        return l + (u - l) * fraction * fraction;
//        return l + (u - l) * std::sqrt(fraction);
        return l + (u - l) * fraction;
    });
    return gs;
}

Matrix LinearCombiner::combine(const Matrix& a, const Matrix& b) {
    Matrix x{a};
    for (int i = 0; i < x.getRowCount(); ++i) {
        x(i, 0) += (b(i, 0) - a(i, 0)) * distribution(GENERATOR);
    }
    return x;
}

std::vector<int> Genetic::sample_3(std::vector<double>& weights) const {
    std::discrete_distribution distribution{weights.begin(), weights.end()};
    std::vector<int> v(3);
    v[0] = distribution(GENERATOR);
    do {
        v[1] = distribution(GENERATOR);
    } while (v[1] == v[0]);

    do {
        v[2] = distribution(GENERATOR);
    } while (v[2] == v[0] or v[2] == v[1]);
    std::sort(v.begin(), v.end(), [&](const int a, const int b) {
        return weights[a] > weights[b];
    });
    return v;
}

Matrix
Genetic::optimize(const std::function<double(const Matrix&)>& function, const int dimension, const int population_size,
                  std::pair<double, double> bounds, double p_mutate, bool to_print) const {
    std::uniform_real_distribution uniform;
    std::vector<Matrix> population = generate(population_size, dimension, bounds.first, bounds.second);
    std::vector<double> evaluations(population.size(), 0.0);
    std::transform(population.begin(), population.end(), evaluations.begin(),
                   [&](const Matrix& x) { return function(evaluate(x, bounds.first, bounds.second)); });
    int min_index = std::min_element(evaluations.begin(), evaluations.end()) - evaluations.begin(); // lowkey hacky
    for (int i = 0; i < 50000; ++i) {
//        std::cout << i << std::endl;
        std::vector<double> good = goodness(evaluations);
//        PRINT(good)
        std::vector<int> indices = sample_3(good);
        // indices[0] -- index of an organism with the best goodness (of 3)
        // indices[1] -- index of an organism with the second best goodness (of 3)
        // indices[2] -- index of an organism with the worst goodness (of 3); the organism will get replaced.
        population[indices[2]] = combine(population[indices[0]],
                                         population[indices[1]]); // a is guaranteed to be better
        if (uniform(GENERATOR) <= p_mutate) mutate(population[indices[2]]);
        evaluations[indices[2]] = function(evaluate(population[indices[2]], bounds.first, bounds.second));
        if (to_print and evaluations[indices[2]] < evaluations[min_index]) {
            min_index = indices[2];
            std::cout << i << "\tNew best:\n" << population[min_index] << std::endl;
        }
    }
    return evaluate(population[min_index], bounds.first, bounds.second);
}

std::vector<Matrix> RealGenetic::generate(const int count, const int dim, const double min, const double max) const {
    std::uniform_real_distribution distribution{min, max};
    std::vector<Matrix> population;
    population.reserve(count);
    for (int i = 0; i < count; ++i) {
        Matrix m{dim, 1};
        for (int j = 0; j < dim; ++j) {
            m(j, 0) = distribution(GENERATOR);
        }
        population.push_back(m);
    }
    return population;
}

void RealGenetic::mutate(Matrix& child) const {
    std::normal_distribution gaussian{0.0, 2.0};
    for (int i = 0; i < child.getRowCount(); ++i) {
        for (int j = 0; j < child.getColumnCount(); ++j) {
            child(i, j) += gaussian(GENERATOR);
        }
    }
}

Matrix RealGenetic::evaluate(Matrix a, double, double) const {
    return a;
}

Matrix HeuristicCombiner::combine(const Matrix& a, const Matrix& b) {
    Matrix x{a};
    double t = distribution(GENERATOR);
    x += t * (a - b);
    return x;
}

std::vector<Matrix> BinaryGenetic::generate(const int count, const int dim, const double min, const double max) const {
    std::vector<Matrix> population;
    population.reserve(count);
    std::uniform_int_distribution distribution{0, 1 << precision};
    for (int i = 0; i < count; ++i) {
        Matrix m{dim, 1};
        for (int j = 0; j < dim; ++j) {
            m(j, 0) = distribution(GENERATOR);
        }
        population.push_back(m);
    }
    return population;
}

void BinaryGenetic::mutate(Matrix& a) const {
    std::uniform_int_distribution<unsigned int> distribution{0, static_cast<unsigned int>(1 << precision)};
    for (int i = 0; i < a.getRowCount(); ++i) {
        a(i, 0) = static_cast<int>(a(i, 0)) ^ distribution(GENERATOR);
    }
}

Matrix BinaryGenetic::evaluate(Matrix m, double min, double max) const {
    Matrix result{m};
    for (int i = 0; i < result.getRowCount(); ++i) {
        result(i, 0) = min + (max - min) * (m(i, 0) / (1 << precision));
    }
    return result;
}

Matrix OnePointCombiner::combine(const Matrix& a, const Matrix& b) {
    Matrix result{a};
    for (int i = 0; i < result.getRowCount(); ++i) {
        unsigned int breakpoint = distribution(GENERATOR);
        unsigned int mask = (1 << breakpoint) - 1;
        result(i, 0) = (static_cast<unsigned int>(a(i, 0)) & mask) | (static_cast<unsigned int>(b(i, 0)) & ~mask);
    }
    return result;
}

Matrix MultiPointCombiner::combine(const Matrix& a, const Matrix& b) {
    Matrix result{a};
    for (int i = 0; i < result.getRowCount(); ++i) {
        unsigned int a1 = a(i, 0);
        unsigned int b1 = b(i, 0);
        unsigned int mask = distribution(GENERATOR);
        result(i, 0) = (a1 & b1) | (a1 & mask) | (b1 & ~mask);
    }
    return result;
}
