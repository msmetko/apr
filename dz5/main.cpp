#include "../linalg/Matrix.h"
#include <cmath>
#include "../ode/integrators.h"

void zadatak1() {
    Matrix A = Matrix::fromFile("1_A.txt");
    Matrix B = Matrix::fromFile("1_B.txt");
    Matrix x_0 = Matrix::fromFile("1_start.txt");
    input_function input = [](const double) { return Matrix{{{0.0}, {0.0}}}; };
    input_function real = [&](const double t) {
        return Matrix{{{x_0(0, 0) * std::cos(t) + x_0(1, 0) * std::sin(t)}, {x_0(1, 0) * std::cos(t) -
                                                                             x_0(0, 0) * std::sin(t)}}};
    };
    double start = 0.0;
    double step = 0.01;
    double end = 10.0;
    Euler euler{A, B, x_0, input, start, step};
    BackwardEuler b_euler{A, B, x_0, input, start, step};
    Trapezoidal trapezoidal{A, B, x_0, input, start, step};
    RK4 rk4{A, B, x_0, input, start, step};
    PECE pece{A, B, x_0, input, start, step};
    PECECE pecece{A, B, x_0, input, start, step};
    std::vector<LinearIntegrator*> integrators{
            &euler,
            &b_euler,
            &trapezoidal,
            &rk4,
            &pece,
            &pecece
    };
    for (auto integrator: integrators) {
        Matrix next{x_0};
        int k = 0;
        double sum = 0.0;
        std::cout << k * step << "\t" << sum << std::endl << next << std::endl;
        double t;
        do {
            t = start + k * step;
            next = integrator->step();
            k++;
            Matrix actual = real(t);
            sum += std::abs(next(0, 0) - actual(0, 0)) + std::abs(next(1, 0) - actual(1, 0));
            std::cout << k * step << "\t" << sum << std::endl << next << std::endl;
        } while ((end - t) > step);
        std::cout << "===" << std::endl;
    }
}

void zadatak2() {
    Matrix A = Matrix::fromFile("2_A.txt");
    Matrix B = Matrix::fromFile("2_B.txt");
    Matrix x_0 = Matrix::fromFile("2_start.txt");
    input_function input = [](const double) { return Matrix{{{0.0}, {0.0}}}; };
    double start = 0.0;
    double step = 0.1;
    double end = 1.0;
    Euler euler{A, B, x_0, input, start, step};
    BackwardEuler b_euler{A, B, x_0, input, start, step};
    Trapezoidal trapezoidal{A, B, x_0, input, start, step};
    RK4 rk4{A, B, x_0, input, start, step};
    PECE pece{A, B, x_0, input, start, step};
    PECECE pecece{A, B, x_0, input, start, step};
    std::vector<LinearIntegrator*> integrators{
            &euler,
            &b_euler,
            &trapezoidal,
            &rk4,
            &pece,
            &pecece
    };
    for (auto integrator: integrators) {
        Matrix next{x_0};
        int k = 0;
        std::cout << k * step << std::endl << next << std::endl;
        double t;
        do {
            t = start + k * step;
            next = integrator->step();
            k++;
            std::cout << k * step << std::endl << next << std::endl;
        } while ((end - t) > step);
        std::cout << "===" << std::endl;
    }
}

void zadatak3() {
    Matrix A = Matrix::fromFile("3_A.txt");
    Matrix B = Matrix::fromFile("3_B.txt");
    Matrix x_0 = Matrix::fromFile("3_start.txt");
    input_function input = [](const double) { return Matrix{{{1.0}, {1.0}}}; };
    double start = 0.0;
    double step = 0.01;
    double end = 10.0;
    Euler euler{A, B, x_0, input, start, step};
    BackwardEuler b_euler{A, B, x_0, input, start, step};
    Trapezoidal trapezoidal{A, B, x_0, input, start, step};
    RK4 rk4{A, B, x_0, input, start, step};
    PECE pece{A, B, x_0, input, start, step};
    PECECE pecece{A, B, x_0, input, start, step};
    std::vector<LinearIntegrator*> integrators{
            &euler,
            &b_euler,
            &trapezoidal,
            &rk4,
            &pece,
            &pecece
    };
    for (auto integrator: integrators) {
        Matrix next{x_0};
        int k = 0;
        double t;
        std::cout << k * step << std::endl << next << std::endl;
        do {
            t = start + k * step;
            next = integrator->step();
            k++;
            std::cout << k * step << std::endl << next << std::endl;
        } while ((end - t) > step);
        std::cout << "===" << std::endl;
    }
}

void zadatak4() {
    Matrix A = Matrix::fromFile("4_A.txt");
    Matrix B = Matrix::fromFile("4_B.txt");
    Matrix x_0 = Matrix::fromFile("4_start.txt");
    input_function input = [](const double t) { return Matrix{{{t}, {t}}}; };
    double start = 0.0;
    double step = 0.01;
    double end = 1.0;
    Euler euler{A, B, x_0, input, start, step};
    BackwardEuler b_euler{A, B, x_0, input, start, step};
    Trapezoidal trapezoidal{A, B, x_0, input, start, step};
    RK4 rk4{A, B, x_0, input, start, step};
    PECE pece{A, B, x_0, input, start, step};
    PECECE pecece{A, B, x_0, input, start, step};
    std::vector<LinearIntegrator*> integrators{
            &euler,
            &b_euler,
            &trapezoidal,
            &rk4,
            &pece,
            &pecece
    };
    for (auto integrator: integrators) {
        Matrix next{x_0};
        int k = 0;
        double t;
        std::cout << k * step << std::endl << next << std::endl;
        do {
            t = start + k * step;
            next = integrator->step();
            k++;
            std::cout << k * step << std::endl << next << std::endl;
        } while ((end - t) > 0.02);
        std::cout << "===" << std::endl;
    }
}

int main() {
//    zadatak1();
//    zadatak2();
    zadatak3();
//    zadatak4();
    return 0;
}