from matplotlib import pyplot as plt

timestamps = []
x1_points = []
x2_points = []
row = 1
while True:
    try:
        first = input().strip()
    except:
        plt.show()
        break
    if '=' in first:
        plt.subplot(3, 2, row)
        plt.plot(timestamps, x1_points)
        plt.plot(timestamps, x2_points)
        row += 1
        x1_points.clear()
        x2_points.clear()
        timestamps.clear()
        continue
    timestamp = float(first.strip())
    x1 = float(input().strip())
    x2 = float(input().strip())
    timestamps.append(timestamp)
    x1_points.append(x1)
    x2_points.append(x2)
# plt.show()
