//
// Created by msmetko on 14. 10. 2019..
//
#include <algorithm>
#include <stdexcept>
#include <iostream>
#include "Matrix.h"

bool test_Matrix() {
    Matrix A({{1.0, 2.0},
              {3.0, 4.0}});
    Matrix B({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    Matrix C({{1.0, 2.0},
              {3.0, 4.0},
              {5.0, 6.0}});
    try {
        Matrix D({{5.0,  6.0},
                  {7.0,  8.0, 9.0},
                  {10.0, 11.0}});
        return false;
    } catch (...) {
    }

    Matrix D(4, 4);
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            if (D(i, j) != 0.0) return false;
        }
    }

    Matrix E(A);
    return !(A != E);
}

bool test_Matrix_getRowCount() {
    Matrix A({{1.0, 2.0},
              {3.0, 4.0},
              {5.0, 6.0}});
    Matrix B({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    return !(A.getRowCount() != 3 or B.getRowCount() != 2);
}

bool test_Matrix_getColumnCount() {
    Matrix A({{1.0, 2.0},
              {3.0, 4.0},
              {5.0, 6.0}});
    Matrix B({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    return !(A.getColumnCount() != 2 or B.getColumnCount() != 3);
}

//bool test_Matrix_operatorEquals() {
//
//}

bool test_Matrix_operatorFunctionCall() {
    Matrix A({{1.0, 2.0},
              {3.0, 4.0},
              {5.0, 6.0}});
    return A(0, 0) == 1.0 and
           A(0, 1) == 2.0 and
           A(1, 0) == 3.0 and
           A(1, 1) == 4.0 and
           A(2, 0) == 5.0 and
           A(2, 1) == 6.0;
}

bool test_Matrix_operatorAddTo() {
    Matrix A({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    Matrix B({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    Matrix C({{1.0, 2.0},
              {3.0, 4.0},
              {5.0, 6.0}});
    A += B;
    bool test;
    test = A(0, 0) == 2.0 and
           A(0, 1) == 4.0 and
           A(0, 2) == 6.0 and
           A(1, 0) == 8.0 and
           A(1, 1) == 10.0 and
           A(1, 2) == 12.0;
    if (!test) return false;
    test = B(0, 0) == 1.0 and
           B(0, 1) == 2.0 and
           B(0, 2) == 3.0 and
           B(1, 0) == 4.0 and
           B(1, 1) == 5.0 and
           B(1, 2) == 6.0;
    if (!test) return false;
    try {
        B += C;
    } catch (std::invalid_argument &) {
        // nice
    } catch (...) {
        return false;
    }
    return true;
}

bool test_Matrix_operatorMultiplyTo() {
    Matrix A({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    A *= -2;
    return A(0, 0) == -2.0 and
           A(0, 1) == -4.0 and
           A(0, 2) == -6.0 and
           A(1, 0) == -8.0 and
           A(1, 1) == -10.0 and
           A(1, 2) == -12.0;
}

bool test_Matrix_math_etc() {
    Matrix A({{1.0, 2.0, 3.0},
              {4.0, 5.0, 6.0}});
    Matrix B(A);
    Matrix C = A + B;
    Matrix D = 2.0 * A;
    Matrix E = B * 2.0;
    if (&C == &D) return false;
    if (&C == &E) return false;
    if (&D == &E) return false;
//    std::cout << C;
//    std::cout << D;
//    std::cout << E;
    if (C != D) return false;
//    std::cout << "OMG" << std::endl;
    if (C != E) return false;
    return !(D != E);
}

bool test_Matrix_fromFile_toFile() {
    Matrix A = Matrix::fromFile("/home/msmetko/fer/apr/matrica1.txt");
    Matrix B = Matrix::fromFile("/home/msmetko/fer/apr/matrica2.txt");
    Matrix C = A * B;
    C.toFile("/home/msmetko/fer/apr/matrica3.txt");
//    std::cout << "OK?" << std::endl;
//    std::cout << C;
    C = Matrix::fromFile("/home/msmetko/fer/apr/matrica3.txt");
    return C == Matrix::eye(4);
}

bool test_Matrix_solve() {
    Matrix A({{1.0, 1.0, 1.0},
              {0.0, 2.0, 5.0},
              {2.0, 5.0, -1.0}});
    Matrix B(A);
    Matrix b({{6.0},
              {-4.0},
              {27.0}});
    Matrix c(b);
    Matrix solution({{5.0},
                     {3.0},
                     {-2.0}});
    Matrix x = Matrix::solve(A, b);
    // solve should make and return copies, and not change these variables
    if (A != B) return false;
    if (b != c) return false;
//    std::cout << x << std::endl;
    return x == solution;
}

bool test_Matrix_solveLUP() {
    Matrix A({{3.0, 9.0,  6.0},
              {4.0, 12.0, 12.0},
              {1.0, -1.0, 1.0}});
    Matrix b({{12.0},
              {12.0},
              {1.0}});
    try {
        Matrix x = Matrix::solve(A, b, false);
    } catch (std::runtime_error&) {
        // OKAY
    } catch (...) {
        return false;
    }
    Matrix solution({{3.0},
                     {1.0},
                     {-1.0}});
    Matrix x = Matrix::solve(A, b, true);
    return solution == x;
}

bool test_Matrix_load_solve() {
    Matrix A = Matrix::fromFile("/home/msmetko/CLionProjects/apr/dz1/4_matrix_A.txt");
    Matrix b = Matrix::fromFile("/home/msmetko/CLionProjects/apr/dz1/4_vector_b.txt");
//    std::cout << "4. false\n" << Matrix::solve(A, b) << std::endl;
//    std::cout << "4. true\n" << Matrix::solve(A, b, true) << std::endl;
    return true;
}

bool test_Matrix_invert() {
    Matrix M({{4.0,  -5.0, -2.0},
              {5.0,  -6.0, -2.0},
              {-8.0, 9.0,  3.0}});
    Matrix i_M({{0.0,  -3.0, -2.0},
                {1.0,  -4.0, -2.0},
                {-3.0, 4.0,  1.0}});
    return ~M == i_M;
}

bool test_Matrix_det() {
    Matrix M({{4.0,  -5.0, -2.0},
              {5.0,  -6.0, -2.0},
              {-8.0, 9.0,  3.0}});
    if (M.det() != 1) return false;
    M = Matrix({{3.0, 9.0,  6.0},
                {4.0, 12.0, 12.0},
                {1.0, -1.0, 1.0}});
    if (M.det() != 48) return false;
    if (Matrix({{1.0, 2.0},
                {3.0, 4.0}}).det() != -2)
        return false;
    return true;
}

bool test() {
    //bool (*tests[])(void) = {test_Matrix, test_Matrix_getRowCount, test_Matrix_getColumnCount};
    //return std::all_of(tests, tests + 3, [](bool (*fun[])(void)) { return (*fun)(); });
    std::vector<bool (*)(void)> functions;
    functions.push_back(test_Matrix);
    functions.push_back(test_Matrix_getRowCount);
    functions.push_back(test_Matrix_getColumnCount);
    functions.push_back(test_Matrix_operatorFunctionCall);
    functions.push_back(test_Matrix_operatorAddTo);
    functions.push_back(test_Matrix_operatorMultiplyTo);
    functions.push_back(test_Matrix_math_etc);
    functions.push_back(test_Matrix_fromFile_toFile);
    functions.push_back(test_Matrix_solve);
    functions.push_back(test_Matrix_solveLUP);
    functions.push_back(test_Matrix_load_solve);
    functions.push_back(test_Matrix_invert);
    functions.push_back(test_Matrix_det);
    return std::all_of(functions.begin(), functions.end(), [](const auto fun) { return fun(); });
}
