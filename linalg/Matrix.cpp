//
// Created by msmetko on 14. 10. 2019..
//

#include "Matrix.h"

#include <utility>
#include <algorithm>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <numeric>

//#define PRINT(v) std::cout << "P "; for(int i: v) {std::cout << i << " ";} std::cout << std::endl;

// stackoverflow
std::string &ltrim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
    str.erase(0, str.find_first_not_of(chars));
    return str;
}

// stackoverlow
std::string &rtrim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
    str.erase(str.find_last_not_of(chars) + 1);
    return str;
}

//stackoverflow
std::string trim(std::string &str, const std::string &chars = "\t\n\v\f\r ") {
    return ltrim(rtrim(str, chars), chars);
}

// stackoverflow
std::vector<std::string> separate(std::string s, const std::string &delimiter) {
    std::vector<std::string> parts;
    size_t pos = 0;
    std::string token;
    while ((pos = s.find(delimiter)) != std::string::npos) {
        token = s.substr(0, pos);
        parts.push_back(token);
        s.erase(0, pos + delimiter.length());
    }
    parts.push_back(s);
    return parts;
}

// stackoverflow
std::vector<double> stringVectorToDoubleVector(std::vector<std::string> stringVector) {
    std::vector<double> doubleVector(stringVector.size());
    std::transform(stringVector.begin(), stringVector.end(), doubleVector.begin(), [](std::string val) {
        return std::stod(trim(val));
    });
    return doubleVector;
}


Matrix::Matrix(std::vector<std::vector<double>> rows) : rows_(std::move(rows)) {
    if (!std::all_of(rows_.begin(), rows_.end(),
                     [&](const auto &row) { return row.size() == rows_[0].size(); })) {
        throw std::invalid_argument("Not all vectors are of the same size!");
    }
}

int Matrix::getRowCount() const {
    return this->rows_.size();
}

int Matrix::getColumnCount() const {
    return this->rows_[0].size();
}

double &Matrix::operator()(int i, int j) {
    return this->rows_[i][j];
}

double Matrix::operator()(int i, int j) const {
    return rows_[i][j];
}

Matrix& Matrix::operator+=(const Matrix& m) {
    if (this->getColumnCount() != m.getColumnCount() && this->getRowCount() != m.getColumnCount()) {
        throw std::invalid_argument("Matrices could not be added: (" + std::to_string(this->getRowCount()) + ", " +
                                    std::to_string(this->getColumnCount()) + ") with (" +
                                    std::to_string(m.getRowCount()) + ", " + std::to_string(m.getColumnCount()) + ").");
    }
    for (int i = 0; i < this->getRowCount(); ++i) {
        for (int j = 0; j < this->getColumnCount(); ++j) {
            (*this)(i, j) += m(i, j);
        }
    }
    return *this;
}

Matrix Matrix::operator+(const Matrix& m) const {
    if (this->getRowCount() == m.getRowCount() and this->getColumnCount() == m.getColumnCount()) {
        Matrix A(*this);
        return A += m;
    } else {
        throw std::invalid_argument("Matrices could not be added: (" + std::to_string(this->getRowCount()) + ", " +
                                    std::to_string(this->getColumnCount()) + ") with (" +
                                    std::to_string(m.getRowCount()) + ", " + std::to_string(m.getColumnCount()) + ").");
    }
}

Matrix &Matrix::operator*=(double c) {
    for (int i = 0; i < this->getRowCount(); ++i) {
        for (int j = 0; j < this->getColumnCount(); ++j) {
            (*this)(i, j) *= c;
        }
    }
    return *this;
}

bool Matrix::operator==(const Matrix &m) const {
    if (this == &m) return true;
    if (this->getRowCount() != m.getRowCount() or this->getColumnCount() != m.getColumnCount()) return false;
    for (int i = 0; i < this->getRowCount(); ++i) {
        for (int j = 0; j < this->getColumnCount(); ++j) {
            if (std::abs((*this)(i, j) - m(i, j)) > LINALG_EPS) {
                return false;
            }
        }
    }
    return true;
}

Matrix::Matrix(int rowCount, int columnCount) : rows_(rowCount, std::vector<double>(columnCount, 0.0)) {
}

bool Matrix::operator!=(Matrix &m) const {
    return !(*this == m);
}

Matrix operator*(const Matrix &m, double c) {
    return Matrix(m) *= c;
}

Matrix operator*(double c, const Matrix &m) {
    return m * c;
}

auto operator*(const Matrix &a, const Matrix &b) -> Matrix {
    if (a.getColumnCount() != b.getRowCount()) {
        throw std::invalid_argument("Matrices are not chained: (" + std::to_string(a.getRowCount()) + ", " +
                                    std::to_string(a.getColumnCount()) + ") with (" + std::to_string(b.getRowCount()) +
                                    ", " + std::to_string(b.getColumnCount()) + ").");
    }
    Matrix M(a.getRowCount(), b.getColumnCount());
//    std::cout << M;
    for (int i = 0; i < M.getRowCount(); ++i) {
        for (int j = 0; j < M.getColumnCount(); ++j) {
            for (int k = 0; k < a.getColumnCount(); ++k) {
                M(i, j) += a(i, k) * b(k, j);
            }
        }
    }
    return M;
}

auto operator<<(std::ostream &os, const Matrix &m) -> std::ostream & {
    for (int i = 0; i < m.getRowCount(); ++i) {
        for (int j = 0; j < m.getColumnCount(); ++j) {
            if (j > 0) os << " ";
            os << m(i, j);
        }
        if ((i + 1) < m.getRowCount()) os << "\n";
    }
    return os;
}

Matrix Matrix::operator~() const {
    Matrix I = Matrix::eye(this->getColumnCount());
    return Matrix::solve(*this, I, true);
}

Matrix Matrix::T() const {
    std::vector<std::vector<double>> matrix;
    for (int j = 0; j < this->getColumnCount(); ++j) {
        std::vector<double> row;
        row.reserve(this->getRowCount());
        for (int i = 0; i < this->getRowCount(); ++i) {
            row.push_back((*this)(i, j));
        }
        matrix.push_back(row);
    }
    return Matrix(matrix);
}

Matrix Matrix::eye(int dim) {
    std::vector<double> row(dim, 0.0);
    row[0] = 1.0;
    std::vector<std::vector<double>> matrix;
    for (int i = 0; i < dim - 1; ++i) {
        matrix.push_back(row);
        std::rotate(row.begin(), row.begin() + dim - 1, row.end());
    }
    matrix.push_back(row);
    return Matrix(matrix);
}

auto operator>>(std::istream &is, Matrix &m) -> std::istream & {
    m.rows_.clear();
    std::string line;
    while (getline(is, line)) {
        line = trim(line);
        auto v = separate(line, " ");
        m.rows_.push_back(stringVectorToDoubleVector(v));
    }
    return is;
}

Matrix Matrix::fromFile(const std::string &uri) {
    std::ifstream file;
    file.open(uri);
    Matrix M(1, 1);
    file >> M;
    return M;
}

void Matrix::toFile(const std::string &uri) const {
    std::ofstream os;
    os.open(uri);
    os << (*this);
    os.close();
}

/**
 * Decomposes M to it's L and U such that L*U = M.
 * L is lower diagonal matrix, with ones on it's diagonal.
 * U is general upper diagonal matrix.
 * @param M         Matrix, to be decomposed
 * @param permute   bool, switches permutations on or off
 *
 * Note that this functions changes the input matrix M!
 */
std::vector<int> decompose(Matrix& M, bool permute = false) {
    std::vector<int> pivot_columns(M.getColumnCount());
    std::iota(pivot_columns.begin(), pivot_columns.end(), 0);
    for (int i = 0; i < M.getRowCount() - 1; ++i) {
        int pivot = i;
        if (permute) {
            for (int j = i + 1; j < M.getColumnCount(); ++j) {
                if (std::abs(M(pivot_columns[j], i)) > std::abs(M(pivot_columns[pivot], i))) {
                    pivot = j;
                }
            }
            std::swap(pivot_columns[i], pivot_columns[pivot]);
        }
        if (std::abs(M(pivot_columns[i], i)) < LINALG_EPS) {
            throw std::runtime_error("Division by zero occured!");
        }
        for (int j = i + 1; j < M.getColumnCount(); ++j) {
            M(pivot_columns[j], i) /= M(pivot_columns[i], i);
            for (int k = i + 1; k < M.getColumnCount(); ++k) {
                M(pivot_columns[j], k) -= M(pivot_columns[j], i) * M(pivot_columns[i], k);
            }
        }
    }
    return pivot_columns;
}


/**
 * Does forward substitution on L decomposed part of a matrix.
 * Workings:
 * * We move downwards over each column of matrix `L`, starting with the first one
 * * Multiply the number in the first column, second row, of the `L` matrix with the first entry in the `b` vector
 * * Subtract that factor from the second entry in the `b` vector.
 * * Repeat the same with the third row of `L` and third entry of `b`
 * * When done, move to the second column of `L` and now multiply those values with the second entry in the `b` vector
 * * Subtract those products from the corresponding entries in the `b` vector.
 * * Repeat until we reach an end.
 *
 * Note that implicitly we do not change the first entry in `b` vector. That's okay.
 * @param L     Matrix, lower triangular matrix we use to update b
 * @param b     Matrix, a column vector of coefficients. Will get updated inplace.
 */
void forward(Matrix& L, Matrix& b, std::vector<int> rowOrder) {
    for (int j = 0; j < L.getColumnCount(); j++) {
        int jj = rowOrder[j];
        for (int i = j + 1; i < L.getRowCount(); i++) {
            int ii = rowOrder[i];
            for (int k = 0; k < b.getColumnCount(); k++) {
                b(ii, k) -= L(ii, j) * b(jj, k);
            }
        }
    }
}

/**
 * Does backward substitution on U decomposed part of a matrix;
 * Workings:
 * * We go upwards over columns, but this time from the last column up to the first one.
 * * First, we calculate the last solution.
 * * Then, we use that solution to update solutions (and implicitly equations, but the calculation is not performed) above
 * * Once we have updated above rows, we move to one solution above.
 * * Repeat the calculation of the second-to-last solution and update of other solutions above until the end.
 *
 * @param U     Matrix, upper triangular matrix we use to update x
 * @param x     Matrix, a column vector, preferably from forward substitution :) Will get updated inplace.
 */
void backward(Matrix& U, Matrix& x, std::vector<int> rowOrder) {
    for (int i = U.getRowCount() - 1; i >= 0; i--) {
        int ii = rowOrder[i];
        for (int kk = 0; kk < x.getColumnCount(); kk++) {
//        std::cout << "I divide with " << U(ii, i) << " in round " << ii << std::endl;
            x(ii, kk) /= U(ii, i);
            for (int j = 0; j < i; ++j) {
                int jj = rowOrder[j];
                x(jj, kk) -= U(jj, i) * x(ii, kk);
            }
        }
    }
}

Matrix Matrix::solve(const Matrix& A, const Matrix& b, const bool permute) {
    Matrix LU(A);
    Matrix y(b);
    Matrix x(y);
    std::vector<int> rowOrder = decompose(LU, permute);
//    PRINT(rowOrder)
//    std::cout << "LU\n" << LU;
    forward(LU, y, rowOrder);
//    std::cout << "Nakon forward\n" << y << "\n";
    backward(LU, y, rowOrder);
//    std::cout << y << "\n";
    for (int k = 0; k < b.getColumnCount(); k++) {
        int i = 0;
        for (int index: rowOrder) {
            x(i++, k) = y(index, k);
        }
    }
    return x;
}

double Matrix::det() const {
    Matrix M(*this);
    auto rowOrder = decompose(M, true);
    double D = 1;
    for (int i = 0; i < M.getColumnCount(); ++i) {
        D *= M(rowOrder[i], i);
    }
    std::vector wished = std::vector(M.getRowCount(), 0.0);
    std::iota(wished.begin(), wished.end(), 0.0);
    int count = 1;
    for (int i = 0; i < wished.size(); i++) {
        if (wished[i] != rowOrder[i]) count++;
    }
    return count % 2 ? -D : D;
}

Matrix& Matrix::operator/=(double c) {
    return (*this) *= 1.0 / c;
}

Matrix& Matrix::operator-=(const Matrix& m) {
    Matrix M(m);
    M *= -1.0;
    return (*this) += M;
}

Matrix Matrix::operator-(const Matrix& other) const {
    Matrix m(*this);
    return m -= other;
}

Matrix Matrix::operator/(const double c) {
    Matrix m(*this);
    return m /= c;
}
