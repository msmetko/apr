//
// Created by msmetko on 14. 10. 2019..
//

#ifndef APR_MATRIX_H
#define APR_MATRIX_H

#define LINALG_EPS 1e-6
#include <vector>
#include <iostream>

/**
 * Matrix class
 *
 * This class is a basic implementation of linear algebra matrices.
 * It is row oriented, which means it follows C convention.
 * Every row is another std::vector<>
 * This will prove to be a bottleneck in the future for some of the methods, but this is a simple asignment after all.
 */
class Matrix {
private:
    std::vector<std::vector<double>> rows_;
public:
    explicit Matrix(std::vector<std::vector<double>>);

    Matrix(const Matrix &m) = default;
    Matrix(int, int);

    Matrix &operator=(const Matrix &m) = default;
    double &operator()(int, int);

    double operator()(int i, int j) const;

    Matrix operator+(const Matrix&) const;

    friend auto operator*(const Matrix &, double) -> Matrix;

    friend auto operator*(double, const Matrix &) -> Matrix;

    friend auto operator*(const Matrix &, const Matrix &) -> Matrix;

    Matrix operator~() const;

    Matrix& operator+=(const Matrix&);

    Matrix operator-(const Matrix&) const;

    Matrix& operator*=(double);

    Matrix& operator/=(double);

    Matrix operator/(double);

    Matrix& operator-=(const Matrix&);

    bool operator==(const Matrix &) const;

    bool operator!=(Matrix &) const;

    friend auto operator<<(std::ostream &, const Matrix &) -> std::ostream &;

    friend auto operator>>(std::istream &, Matrix &) -> std::istream &;

    Matrix T() const;

    double det() const;

    static Matrix eye(int);

    static Matrix fromFile(const std::string &);

    void toFile(const std::string &) const;

    int getRowCount() const;

    int getColumnCount() const;

    static Matrix solve(const Matrix&, const Matrix&, bool= false);
};


#endif //APR_MATRIX_H
