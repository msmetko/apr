//
// Created by msmetko on 02. 11. 2019..
//
//#include "../function/Function.h"
#include "../optim/optim.h"
#include <iostream>
#include <random>

static bool zadatak1() {
    std::cout << "Z1\n" << std::endl;
    double min;
    Matrix x{{{10}}};
    Matrix dx{{{1.0}}};
    for (int i = 0; i < 5; ++i) {
        Matrix x0(x);
        x0(0, 0) += i * 5;
        Function<Matrix, double> f([](const Matrix& x) {
            //return 10 - std::exp(-(x(0, 0) - 3)*(x(0, 0) - 3));
            return (x(0, 0) - 3) * (x(0, 0) - 3);
        });
        Function<double, double> help_f([&](double x) { return f(Matrix{{{x}}}); });
        min = golden_section_search(help_f, x0(0, 0));
        std::cout << "Golden section:\t" << min << " (" << help_f(min) << ")\nCalls: " << help_f.getCallCount() << "\n";
        f.resetCallCount();

        Matrix m_min = coordinate_descent(f, x0);
        std::cout << "Coord descent:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        m_min = amoeba_simplex(f, x0);
        std::cout << "Amoeba simplex:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        m_min = hooke_jeeves_pattern_search(f, x0, dx);
        std::cout << "Hooke-jeeves:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        std::cout << "===" << std::endl;
    }
//    std::cout << std::endl;
    return true;
}

static bool zadatak2() {
    std::cout << "\nZ2\n" << std::endl;
    std::vector<Matrix> starting_points{
            Matrix{{{-1.9}, {2.0}}},
            Matrix{{{0.1}, {0.3}}},
            Matrix{std::vector<std::vector<double>>(6, std::vector<double>(1, 0.0))},
            Matrix{{{5.1}, {1.1}}}
    };
    std::vector<Function<Matrix, double>> functions{
            F_1,
            F_2,
            F_3,
            F_4
    };
    for (int i = 0; i < functions.size(); ++i) {
        std::cout << "F" << (i + 1) << std::endl;
        auto f = functions[i];
        auto start = starting_points[i];
        Matrix dx(std::vector<std::vector<double>>(start.getRowCount(), std::vector<double>(1.0, 1.0)));
        Matrix m_min = coordinate_descent(f, start);
        std::cout << "Coord descent:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        m_min = amoeba_simplex(f, start);
        std::cout << "Amoeba simplex:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        m_min = hooke_jeeves_pattern_search(f, start, dx);
        std::cout << "Hooke-jeeves:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
        f.resetCallCount();

        std::cout << "===" << std::endl;
    }
    return true;
}

static bool zadatak3() {
    std::cout << "\nZ3\n" << std::endl;
    Function f = F_4;
    Matrix start{{{5.0}, {5.0}}};
    Matrix dx{{{1.0}, {1.0}}};
    Matrix m_min = amoeba_simplex(f, start);
    std::cout << "Amoeba simplex:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
    f.resetCallCount();

    m_min = hooke_jeeves_pattern_search(f, start, dx);
    std::cout << "Hooke-jeeves:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << "\n";
    f.resetCallCount();
    return true;
}

static bool zadatak4() {
    std::cout << "\nZ4\n" << std::endl;
    Matrix start{{{0.5}, {0.5}}};
    Function f = F_1;
    for (int i = 1; i <= 20; ++i) {
        Matrix m_min = amoeba_simplex(f, start, i);
        std::cout << "Amoeba simplex:\t" << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << " (Steps="
                  << i << ")\n";
        f.resetCallCount();
    }
    return 4;
}

static bool zadatak5() {
    std::cout << "\nZ5\n" << std::endl;
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> random(-50, 50);
    Matrix solution{{{1.0}, {2.0}}};
    Function f = F_6;
    int count;
    for (int i = 0; i < 100; ++i) {
        double x = random(gen);
        double y = random(gen);
        Matrix start{{{x}, {y}}};
        std::cout << start << std::endl;
        Matrix dx{{{1.0}, {1.0}}};
//        Matrix m_min = hooke_jeeves_pattern_search(f, start, dx); std::cout << "Hooke-jeves:\t"
        Matrix m_min = amoeba_simplex(f, start, 1);
        std::cout << "Amoeba simples:\t"
                  << m_min << " (" << f(m_min) << ")\nCalls: " << f.getCallCount() << ")\n";
        f.resetCallCount();
        Matrix diff = m_min - solution;
        if (std::sqrt((diff.T() * diff)(0, 0)) < 1e-4) ++count;
    }
    std::cout << "\n" << count << "/100" << std::endl;
    return true;
}

static bool test() {
    return zadatak1() and zadatak2() and zadatak3() and zadatak4() and zadatak5();
}