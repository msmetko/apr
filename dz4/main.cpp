//
// Created by msmetko on 12. 12. 2019..
//

#include <iostream>
#include <numeric>
#include "../genalg/genetic.h"

/* FUNCTION 1 */
std::function<double(const Matrix&)> f_1([](const Matrix& x) {
    return 100 * (x(1, 0) - x(0, 0) * x(0, 0)) * (x(1, 0) - x(0, 0) * x(0, 0)) + (1 - x(0, 0)) * (1 - x(0, 0));
});

std::function<double(const Matrix&)> f_3([](const Matrix& x) {
    double sum = 0.0;
    for (int i = 0; i < x.getRowCount(); ++i) {
        double t = x(i, 0) - i - 1;
        sum += t * t;
    }
    return sum;
});

std::function<double(const Matrix&)> f_6([](const Matrix& x) {
    double sum_sq = (x.T() * x)(0, 0);
    double s = std::sin(std::sqrt(sum_sq));
    return 0.5 + (s * s - 0.5) / (1 + 0.001 * sum_sq);
});

std::function<double(const Matrix&)> f_7([](const Matrix& x) {
    double sum_sq = (x.T() * x)(0, 0);
    double C = std::pow(sum_sq, 0.25);
    double s = std::sin(50 * std::pow(sum_sq, 0.1));
    return C * (1 + s * s);
});

std::vector<std::function<double(const Matrix&)>> FUNCTIONS = {
        f_1,
        f_3,
        f_6,
        f_7
};

void zadatak1() {
    for (int i = 0; i < FUNCTIONS.size(); ++i) {
        auto& F = FUNCTIONS[i];
        std::cout << (i == 0 ? "f_1" : (i == 1 ? "\nf_3" : (i == 2 ? "\nf_6" : "\nf_7")));
        LinearCombiner lc{0, 1};
        RealGenetic rg1{lc};
        Matrix sol = rg1.optimize(F, i == 1 ? 5 : 2, 25, {-50, 150}, 0.9, false);
        std::cout << "\nReal (linear)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
        HeuristicCombiner hc;
        RealGenetic rg2{hc};
        sol = rg2.optimize(F, i == 1 ? 5 : 2, 25, {-50, 150}, 0.9, false);
        std::cout << "\nReal (heuristic)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
        int precision = 8;
        OnePointCombiner opc{precision};
        BinaryGenetic bg1{opc, precision};
        sol = bg1.optimize(F, i == 1 ? 5 : 2, 25, {-50, 150}, 0.8, false);
        std::cout << "\nBinary (one point)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
        MultiPointCombiner mpc{precision};
        BinaryGenetic bg2{mpc};
        sol = bg2.optimize(F, i == 1 ? 5 : 2, 25, {-50, 150}, 0.8, false);
        std::cout << "\nBinary (multi point)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
    }
}

void zadatak2() {
    std::vector<std::function<double(const Matrix&)>> functions = {f_6, f_7};
    std::vector<int> dimensions = {1, 3, 6, 10};
    int precision = 16;
    for (int i = 0; i < functions.size(); ++i) {
        std::cout << (i == 0 ? "f_6" : "f_7") << std::endl;
        auto& F = functions[i];
        for (auto dim: dimensions) {
            LinearCombiner lc{0, 1};
            RealGenetic rg1{lc};
            Matrix sol = rg1.optimize(F, i == 1 ? 5 : 2, 50, {-50, 150}, 0.7, false);
            std::cout << "dim=" << dim << "\nReal (linear)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
            HeuristicCombiner hc;
            RealGenetic rg2{hc};
            sol = rg2.optimize(F, i == 1 ? 5 : 2, 50, {-50, 150}, 0.7, false);
            std::cout << "dim=" << dim << "\nReal (heuristic)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
            OnePointCombiner opc{precision};
            BinaryGenetic bg1{opc, precision};
            sol = bg1.optimize(F, i == 1 ? 5 : 2, 50, {-50, 150}, 0.35, false);
            std::cout << "dim=" << dim << "\nBinary (one point)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
            MultiPointCombiner mpc{precision};
            BinaryGenetic bg2{mpc};
            sol = bg2.optimize(F, i == 1 ? 5 : 2, 50, {-50, 150}, 0.5, false);
            std::cout << "dim=" << dim << "\nBinary (multi point)\tf(x) = " << F(sol) << std::endl << sol << std::endl;
        }
        std::cout << std::endl;
    }
}

void zadatak3() {
    zadatak2();
}

void zadatak4() {
    auto& F = f_6;
    LinearCombiner lc{-0.5, 1.5};
    RealGenetic rg1{lc};
    Matrix sol = rg1.optimize(F, 2, 50, {-50, 150}, 0.5, false);
    std::cout << F(sol) << std::endl << sol;
}

int main() {
//    zadatak1();
//    zadatak2();
    zadatak4();
    return 0;
}