//
// Created by msmetko on 28. 10. 2019..
//

#ifndef APR_FUNCTION_H
#define APR_FUNCTION_H

#include <functional>
#include <cmath>
#include "../linalg/linalg.h"

template<typename In, typename Out>
class Function {
private:
    int calls_;
    const std::function<Out(In)> f_;
public:
    explicit Function(const std::function<Out(In)>& f) : f_(f), calls_(0) {};

    Out operator()(In input);

    int getCallCount() const;

    void resetCallCount();
};

template<typename In, typename Out>
Out Function<In, Out>::operator()(In input) {
    ++calls_;
    return f_(input);
}

template<typename In, typename Out>
int Function<In, Out>::getCallCount() const {
    return calls_;
}

template<typename In, typename Out>
void Function<In, Out>::resetCallCount() {
    calls_ = 0;
}

static Function<Matrix, double> F_1([](const Matrix& x) {
    return 100 * (x(1, 0) - x(0, 0) * x(0, 0)) * (x(1, 0) - x(0, 0) * x(0, 0)) + (1 - x(0, 0)) * (1 - x(0, 0));
});

static Function<Matrix, double> F_2([](const Matrix& x) {
    return (x(0, 0) - 4) * (x(0, 0) - 4) + 4 * (x(1, 0) - 2) * (x(1, 0) - 2);
});

static Function<Matrix, double> F_3([](const Matrix& x) {
    double sum = 0.0;
    for (int i = 0; i < x.getRowCount(); ++i) {
        sum += (x(i, 0) - i - 1) * (x(i, 0) - i - 1);
    }
    return sum;
});

static Function<Matrix, double> F_4([](const Matrix& X) {
    double x = X(0, 0);
    double y = X(1, 0);
    return std::abs((x - y) * (x + y)) + std::hypot(x, y);
});

static Function<Matrix, double> F_6([](const Matrix& x) {
    double sum_sq = (x.T() * x)(0, 0);
    double s = std::sin(std::sqrt(sum_sq));
    return 0.5 + (s * s - 0.5) / (1 + 0.001 * sum_sq) / (1 + 0.001 * sum_sq);
});
#endif //APR_FUNCTION_H
